<?php $this->extend('template/template'); ?>
<?php $this->section('isi'); ?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body>
      <div class="container">          
          <div class="card mt-3">
              <div class="card-header">
                  <b><?= $title ?></b>
          </div>
          <div class="card-body">
            <form action="<?php echo base_url('mahasiswa/simpan')?>" method="post">
                <div class="form-group row">
                    <label for="nama" class="col-sm-2 label">Nama</label>
                    <div class="col-sm-4">
                        <input type="text" name="nama" class="form-control">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="nama" class="col-sm-2 label">Jenis Kelamin</label>
                    <div class="col-sm-4">
                        <input type="text" name="jenis_kelamin" class="form-control">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="nama" class="col-sm-2 label">Alamat</label>
                    <div class="col-sm-5">
                        <input type="text" name="alamat" class="form-control">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-4">
                        <input type="submit" value="submit" class="btn btn-info">
                    </div>
            </form>
                </div>            
        </div>
    </div>
    </div>
			<?php $this->endSection(); ?>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    -->
  </body>
</html>