<?php $this->extend('template/template'); ?>
<?php $this->section('isi'); ?>
<div class="container">
    <div class="card mt-3">
        <div class="card-header">
            <b><?= $title ?></b>
        </div>
        <div class="card-body">
            <a href="mahasiswa/tambah" class="btn btn-info">Tambah </a>
            <br><br>
            <table class="table table-bordered">
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Jenis Kelamin</th>
                    <th>Alamat</th>
                    <th>Aksi</th>
                </tr>
                <?php
                $no = 1;
                foreach ($mahasiswa as $key) : ?>
                    <tr>
                        <td><?php echo $no++; ?></td>
                        <td><?php echo $key['nama']; ?></td>
                        <td><?php echo $key['jenis_kelamin']; ?></td>
                        <td><?php echo $key['alamat']; ?></td>
                        <td>
                            <a href="/mahasiswa/edit/<?php echo $key['id']; ?>">Edit</a>
                            <a href="/mahasiswa/delete/<?php echo $key['id']; ?>">Hapus</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
    </div>
</div>
<?php $this->endSection(); ?>