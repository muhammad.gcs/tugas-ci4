<?php namespace App\Controllers;

use App\models\BungaModel;

class Belajar extends BaseController
{
  public function __construct()
  {
	  $this->BungaModel = new BungaModel();
  }

  public function index()
  {
	  $data["Bunga"] = $this->BungaModel->daftarBunga();
	  $data["Order"] = $this->BungaModel->daftarOrder();
	  $data["Judul"] = "Daftar Bunga";
	  return view('v_bunga',$data);
  }
}