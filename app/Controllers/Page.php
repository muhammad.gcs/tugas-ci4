<?php namespace App\Controllers;

class Page extends BaseController
{
  public function about($name)
  {
    echo "about page ".$name;
  }

  public function contact()
  {
    echo "contact page";
  }

  private function faqs()
  {
    echo "faqs page";
  }

  public function tos()
  {
    echo "Halamana Term and Service";
  }
}