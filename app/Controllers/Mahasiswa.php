<?php

namespace App\Controllers;

use App\Models\MahasiswaModel;

class Mahasiswa extends BaseController
{
	protected $mahasiswamodel;
	public function __construct()
	{
		$this->mahasiswamodel = new MahasiswaModel();
	}

	public function index()
	{
		$mahasiswa = $this->mahasiswamodel->findAll();

		$data = [
			'title' 	=> 'Data Mahasiswa',
			'mahasiswa' => $mahasiswa
		];
		echo view('mahasiswa/data_mahasiswa', $data);
	}
}